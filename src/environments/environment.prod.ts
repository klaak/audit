export const environment = {
  production: true,
  origin: 'https://klaak.gitlab.io',
  api: 'https://klaak-audit.herokuapp.com',
};
