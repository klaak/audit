import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { AuditListComponent } from './audit/audit-list/audit-list.component';
import { NgZorroModule } from '../shared/ng-zorro/ng-zorro.module';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [AuditListComponent],
  imports: [
    CommonModule,
    TranslateModule,
    NgZorroModule,
    RouterModule,
    FlexLayoutModule,
  ],
})
export class ContentModule {}
