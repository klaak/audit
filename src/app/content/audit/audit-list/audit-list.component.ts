import { Component, Injector } from '@angular/core';
import { Request } from '../../../shared/graphql/services/request-enum';
import { Audit } from '../../../shared/graphql/models/collections/audit';
import { AbstractTableComponent } from '../../../shared/ng-zorro/abstract-table-component';
import { PaginationParams } from '../../../shared/graphql/models/utils/pagination-params';

@Component({
  selector: 'app-audit-list',
  templateUrl: './audit-list.component.html',
  styleUrls: ['./audit-list.component.scss'],
})
export class AuditListComponent extends AbstractTableComponent<Audit> {
  defaultSortOrder = 'ascend';
  isInError = false;

  constructor(injector: Injector) {
    super(injector);
  }

  protected refreshData(params?: PaginationParams): void {
    this.isTableLoading = true;
    this.isInError = false;
    const newParams = { ...params };
    this.collectionsService.query(Request.AUDITS, newParams).subscribe({
      next: (data: any) => {
        this.displayedData = data.audits;
        this.isTableLoading = false;
      },
      error: (err) => (this.isInError = true),
    });
  }
}
