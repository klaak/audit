export interface CollectionAggregator {
  count: number;
  totalCount: number;
}
