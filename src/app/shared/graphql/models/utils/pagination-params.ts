export type PaginationParams = {
  limit: number;
  sort?: string;
  start: number;
  where?: any;
};
