import { CollectionAggregator } from './collection-aggregator';

export interface CollectionConnection {
  aggregate: CollectionAggregator;
}
