import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import {
  ApolloError,
  ApolloQueryResult,
  FetchPolicy,
} from '@apollo/client/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Request } from './request-enum';

@Injectable()
export class CollectionsService {
  constructor(
    private apollo: Apollo,
    private nzMessageService: NzMessageService
  ) {}

  query<T>(
    request: Request,
    variables?: { [k: string]: string | number | boolean | object | undefined },
    fetchPolicy?: FetchPolicy
  ): Observable<T> {
    return this.apollo
      .query({
        query: gql`
          ${request}
        `,
        variables,
        fetchPolicy,
      })
      .pipe(
        map((result: ApolloQueryResult<unknown>): T => result.data as T),
        catchError((error: ApolloError) => {
          this.notifyError(error);
          throw error;
        })
      );
  }

  mutate<T>(
    request: Request,
    variables?: { [k: string]: string | number | boolean | object }
  ): Observable<T> {
    return this.apollo
      .mutate({
        mutation: gql`
          ${request}
        `,
        variables,
      })
      .pipe(
        map((result: any): T => result.data),
        catchError((error: ApolloError) => {
          this.notifyError(error);
          throw error;
        })
      );
  }

  private notifyError(error: ApolloError): void {
    this.nzMessageService.error(error.message);
  }
}
