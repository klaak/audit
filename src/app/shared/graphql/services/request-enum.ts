export enum Request {
  AUDITS = `
    query audits($sort: String, $limit: Int, $start: Int) {
      audits(sort: $sort, limit: $limit, start: $start) {
        id
        name
        updatedAt
      }
    }
  `
}
