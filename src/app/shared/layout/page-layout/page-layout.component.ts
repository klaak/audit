import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoginService } from '../../auth/login/login.service';
import { CurrentUserService } from '../../auth/login/current-user.service';
import { Subscription } from 'rxjs';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-page-layout',
  templateUrl: './page-layout.component.html',
  styleUrls: ['./page-layout.component.scss'],
})
export class PageLayoutComponent implements OnInit, OnDestroy {
  isCollapsed = true;
  mediaObserverSubscription: Subscription;
  isSmallScreen: boolean;
  constructor(
    private loginService: LoginService,
    public currentUserService: CurrentUserService,
    private mediaObserver: MediaObserver
  ) {}

  ngOnDestroy(): void {
    this.mediaObserverSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.mediaObserverSubscription = this.mediaObserver
      .asObservable()
      .pipe(
        filter((changes: MediaChange[]) => changes.length > 0),
        map((changes: MediaChange[]) => changes[0])
      )
      .subscribe((mediaChange: MediaChange) => {
        switch (mediaChange.mqAlias) {
          case 'sm':
            this.isSmallScreen = true;
            break;
          case 'xs':
            this.isSmallScreen = true;
            break;
          default:
            this.isSmallScreen = false;
            break;
        }
      });
  }

  login(): void {
    this.loginService.loginPage().then(() => {});
  }

  logout(): void {
    this.loginService.logout();
  }
}
