import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutComponent } from './page-layout/page-layout.component';
import { NgZorroModule } from '../ng-zorro/ng-zorro.module';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { ExtendedModule, FlexModule } from '@angular/flex-layout';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [PageLayoutComponent],
  imports: [
    CommonModule,
    NgZorroModule,
    RouterModule,
    TranslateModule,
    FlexModule,
    ExtendedModule,
    PipesModule,
  ],
  exports: [PageLayoutComponent],
})
export class LayoutModule {}
