import { Directive, Injector, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { Observable } from 'rxjs';
import { CurrentUserService } from '../auth/login/current-user.service';
import { PaginationParams } from '../graphql/models/utils/pagination-params';
import { CollectionsService } from '../graphql/services/collections.service';

@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class AbstractTableComponent<T> implements OnInit {
  displayedData: T[] = [];
  isTableLoading = false;
  pageIndex = 1;
  pageSize = 10;
  total = 0;
  protected collectionsService: CollectionsService;
  protected currentUserService: CurrentUserService;
  protected nzModalService: NzModalService;
  protected tableParams: PaginationParams | undefined;
  protected translateService: TranslateService;

  constructor(injector: Injector) {
    this.collectionsService = injector.get(CollectionsService);
    this.currentUserService = injector.get(CurrentUserService);
    this.nzModalService = injector.get(NzModalService);
    this.translateService = injector.get(TranslateService);
  }

  ngOnInit(): void | Observable<void> {
    if (this.tableParams) {
      this.refreshData(this.tableParams);
    }
  }

  onQueryParamsChange({
    pageSize,
    pageIndex,
    sort,
    filter,
  }: NzTableQueryParams): void {
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;

    const paginationParams: PaginationParams = {
      limit: pageSize,
      start: (pageIndex - 1) * pageSize,
    };

    const currentSort = sort.find((item) => item.value !== null);
    if (currentSort) {
      paginationParams.sort = `${currentSort.key}:${
        currentSort.value === 'ascend' ? 'asc' : 'desc'
      }`;
    }

    const currentFilters = filter?.filter(
      (f) => f.value !== null && (!Array.isArray(f.value) || f.value.length)
    );
    if (currentFilters?.length) {
      paginationParams.where = currentFilters.reduce(
        (json, f) => ({ ...json, [f.key]: f.value }),
        {}
      );
    }

    this.tableParams = paginationParams;
    this.refreshData(paginationParams);
  }

  protected askDeletionConfirmation(content: string): Observable<boolean> {
    return new Observable<boolean>((observer) => {
      const modal = this.nzModalService.create({
        nzTitle: this.translateService.instant('common.actions.deletion'),
        nzContent: content,
        nzFooter: [
          {
            label: this.translateService.instant('common.actions.cancel'),
            onClick: () => {
              modal.destroy();
              observer.next(false);
              observer.complete();
            },
          },
          {
            label: this.translateService.instant('common.actions.confirm'),
            danger: true,
            onClick: () => {
              modal.destroy();
              observer.next(true);
              observer.complete();
            },
          },
        ],
      });
    });
  }

  protected checkPageAndRefresh(): void {
    if (this.pageIndex !== 1) {
      this.pageIndex = 1;
    } else {
      this.refreshData(this.tableParams);
    }
  }

  protected abstract refreshData(params?: PaginationParams): void;
}
