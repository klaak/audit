export interface Me {
  username: string;
  id: string;
  email: string;
  blocked: boolean;
}
