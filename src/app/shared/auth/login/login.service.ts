import { Injectable } from '@angular/core';

import { environment } from '../../../../environments/environment';
import { TokenService } from '../token/token.service';
import { CurrentUserService } from './current-user.service';

@Injectable()
export class LoginService {
  constructor(
    private tokenService: TokenService,
    private currentUserService: CurrentUserService
  ) {}

  loginPage(): Promise<void> {
    return new Promise((resolve, reject) => {
      const onToken = (e: { data: { type: any }; origin: any }) => {
        if (this.tokenService.isTokenKey(e.data.type)) {
          if (e.origin === environment.origin) {
            this.currentUserService.updateMe();
            window.removeEventListener('message', onToken);
            (window as any).loginPopup.close();
            resolve();
          } else {
            reject('BAD_ORIGIN');
          }
        }
      };
      window.addEventListener('message', onToken);
      (window as any).loginPopup = window.open(
        `${environment.api}/connect/google`
      );
    });
  }

  logout(): void {
    this.tokenService.removeToken();
    window.location.reload();
  }
}
