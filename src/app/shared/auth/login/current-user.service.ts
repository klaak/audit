import { Injectable } from '@angular/core';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { Me } from '../me/me';
import { TokenService } from '../token/token.service';

@Injectable()
export class CurrentUserService {
  private meSubject: Subject<Me>;

  get me$(): Observable<Me> {
    return this.meSubject.asObservable();
  }

  constructor(private tokenService: TokenService) {
    this.meSubject = new ReplaySubject(1);
    this.tokenService.extractToken();
    this.updateMe();
  }

  updateMe(): void {
    if (this.tokenService.hasToken()) {
      const user = this.tokenService.getToken().user;
      this.meSubject.next(user);
    }
  }

  private logout(): void {
    this.tokenService.removeToken();
  }
}
