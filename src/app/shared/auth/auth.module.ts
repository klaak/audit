import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginService } from './login/login.service';
import { CurrentUserService } from './login/current-user.service';
import { TokenService } from './token/token.service';
import { TokenInterceptorService } from './token/token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [
    LoginService,
    CurrentUserService,
    TokenService,
    TokenInterceptorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    TokenInterceptorService,
  ],
})
export class AuthModule {}
