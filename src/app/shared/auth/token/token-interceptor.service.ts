import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { TokenService } from './token.service';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private tokenService: TokenService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.tokenService.hasToken()) {
      const jwt = this.tokenService.getToken()?.jwt;
      if (jwt) {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${jwt}`,
          },
        });
      }
    }
    return next.handle(request).pipe(
      tap(
        () => {},
        (err: any) => {
          if (err instanceof HttpErrorResponse && err.status === 401) {
            this.tokenService.removeToken();
          }
        }
      )
    );
  }
}
