import { Me } from '../me/me';

export interface Token {
  jwt: string;
  user: Me;
}
