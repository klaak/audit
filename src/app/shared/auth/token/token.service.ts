import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params } from '@angular/router';
import { Token } from './token';
import { environment } from '../../../../environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { switchMap } from 'rxjs/operators';
import { of, throwError } from 'rxjs';

const TOKEN_KEY = 'TOKEN';

enum LoginError {
  REGISTRATION_DISABLED = 'REGISTRATION_DISABLED',
  USER_BLOCKED = 'USER_BLOCKED',
  UNKNOWN = 'UNKNOWN',
}

@Injectable()
export class TokenService {
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private message: NzMessageService,
    private injector: Injector
  ) {}

  extractToken(): void {
    this.route.queryParams.subscribe((queryParams: Params) => {
      if (queryParams.id_token) {
        this.http
          .get<Token>(
            `${environment.api}/auth/google/callback${location.search}`
          )
          .pipe(
            switchMap((token: Token) => {
              if (token.user.blocked) {
                return throwError(LoginError.USER_BLOCKED.toString());
              }
              return of(token);
            })
          )
          .subscribe({
            next: (token: Token) => {
              this.setToken(token);
              window.opener.postMessage(
                {
                  type: TOKEN_KEY,
                  token,
                },
                environment.origin
              );
            },
            error: (err) => this.handleError(err),
          });
      }
    });
  }

  getToken(): Token {
    const tokenStr = window.localStorage.getItem(TOKEN_KEY);
    return tokenStr ? JSON.parse(tokenStr) : null;
  }

  hasToken(): boolean {
    return !!this.getToken();
  }

  isTokenKey(key: string): boolean {
    return key === TOKEN_KEY;
  }

  removeToken(): void {
    window.localStorage.removeItem(TOKEN_KEY);
  }

  private setToken(token: Token): void {
    window.localStorage.setItem(TOKEN_KEY, JSON.stringify(token));
  }

  private handleError(error: any): void {
    const isError = (strapiErrorLabel): boolean => {
      return error.error.data[0].messages.find(
        (message) => message.id === strapiErrorLabel
      );
    };
    const translate = this.injector.get(TranslateService);
    let errorType: LoginError = LoginError.UNKNOWN;
    if (error?.error?.data?.length) {
      if (isError('Auth.advanced.allow_register')) {
        errorType = LoginError.REGISTRATION_DISABLED;
      }
    } else if (typeof error === 'string') {
      errorType = LoginError.USER_BLOCKED;
    }

    this.message.error(
      translate.instant(`errors.login.${errorType}`, error?.message)
    );
  }
}
