import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'name'
})
export class NamePipe implements PipeTransform {

  transform(contact: { firstName: string, lastName: string }): string {
    return `${contact.firstName} ${contact.lastName}`;
  }

}
