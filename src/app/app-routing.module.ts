import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuditListComponent} from './content/audit/audit-list/audit-list.component';

const routes: Routes = [
  {
    path: 'audit',
    component: AuditListComponent
  },
  {
    path: '**',
    redirectTo: 'audit',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
