import { Component } from '@angular/core';
import { CurrentUserService } from './shared/auth/login/current-user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(public currentUserService: CurrentUserService) {}
}
